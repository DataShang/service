package com.jyxy.service.model;

import com.jyxy.service.model.base.BaseDqcDataCheckTableConfig;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class DqcDataCheckTableConfig extends BaseDqcDataCheckTableConfig<DqcDataCheckTableConfig> {
	public static final DqcDataCheckTableConfig dao = new DqcDataCheckTableConfig().dao();
}
