package com.jyxy.service.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseCmpInvestcmpRecord<M extends BaseCmpInvestcmpRecord<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setRowkey(java.lang.String rowkey) {
		set("rowkey", rowkey);
		return (M)this;
	}
	
	public java.lang.String getRowkey() {
		return getStr("rowkey");
	}

	public M setFCmpId(java.lang.String fCmpId) {
		set("f_cmp_id", fCmpId);
		return (M)this;
	}
	
	public java.lang.String getFCmpId() {
		return getStr("f_cmp_id");
	}

	public M setFInvestedCmpid(java.lang.String fInvestedCmpid) {
		set("f_invested_cmpid", fInvestedCmpid);
		return (M)this;
	}
	
	public java.lang.String getFInvestedCmpid() {
		return getStr("f_invested_cmpid");
	}

	public M setFInvestedLegalPerson(java.lang.String fInvestedLegalPerson) {
		set("f_invested_legal_person", fInvestedLegalPerson);
		return (M)this;
	}
	
	public java.lang.String getFInvestedLegalPerson() {
		return getStr("f_invested_legal_person");
	}

	public M setFInvestedName(java.lang.String fInvestedName) {
		set("f_invested_name", fInvestedName);
		return (M)this;
	}
	
	public java.lang.String getFInvestedName() {
		return getStr("f_invested_name");
	}

	public M setFInvestedCreateTime(java.util.Date fInvestedCreateTime) {
		set("f_invested_create_time", fInvestedCreateTime);
		return (M)this;
	}
	
	public java.util.Date getFInvestedCreateTime() {
		return get("f_invested_create_time");
	}

	public M setFPercentage(java.lang.String fPercentage) {
		set("f_percentage", fPercentage);
		return (M)this;
	}
	
	public java.lang.String getFPercentage() {
		return getStr("f_percentage");
	}

	public M setFInvestedRegisteredCapital(java.math.BigDecimal fInvestedRegisteredCapital) {
		set("f_invested_registered_capital", fInvestedRegisteredCapital);
		return (M)this;
	}
	
	public java.math.BigDecimal getFInvestedRegisteredCapital() {
		return get("f_invested_registered_capital");
	}

	public M setFInvestedAmount(java.math.BigDecimal fInvestedAmount) {
		set("f_invested_amount", fInvestedAmount);
		return (M)this;
	}
	
	public java.math.BigDecimal getFInvestedAmount() {
		return get("f_invested_amount");
	}

	public M setFCurrencyUnit(java.lang.String fCurrencyUnit) {
		set("f_currency_unit", fCurrencyUnit);
		return (M)this;
	}
	
	public java.lang.String getFCurrencyUnit() {
		return getStr("f_currency_unit");
	}

	public M setFInvestedIndustry(java.lang.String fInvestedIndustry) {
		set("f_invested_industry", fInvestedIndustry);
		return (M)this;
	}
	
	public java.lang.String getFInvestedIndustry() {
		return getStr("f_invested_industry");
	}

	public M setFInvestedArea(java.lang.String fInvestedArea) {
		set("f_invested_area", fInvestedArea);
		return (M)this;
	}
	
	public java.lang.String getFInvestedArea() {
		return getStr("f_invested_area");
	}

	public M setFInvestedTime(java.util.Date fInvestedTime) {
		set("f_invested_time", fInvestedTime);
		return (M)this;
	}
	
	public java.util.Date getFInvestedTime() {
		return get("f_invested_time");
	}

	public M setFResource(java.lang.String fResource) {
		set("f_resource", fResource);
		return (M)this;
	}
	
	public java.lang.String getFResource() {
		return getStr("f_resource");
	}

	public M setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
		return (M)this;
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public M setCreateBy(java.lang.String createBy) {
		set("create_by", createBy);
		return (M)this;
	}
	
	public java.lang.String getCreateBy() {
		return getStr("create_by");
	}

	public M setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
		return (M)this;
	}
	
	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public M setUpdateBy(java.lang.String updateBy) {
		set("update_by", updateBy);
		return (M)this;
	}
	
	public java.lang.String getUpdateBy() {
		return getStr("update_by");
	}

	public M setRemarks(java.lang.String remarks) {
		set("remarks", remarks);
		return (M)this;
	}
	
	public java.lang.String getRemarks() {
		return getStr("remarks");
	}

	public M setDelFlag(java.lang.String delFlag) {
		set("del_flag", delFlag);
		return (M)this;
	}
	
	public java.lang.String getDelFlag() {
		return getStr("del_flag");
	}

}
