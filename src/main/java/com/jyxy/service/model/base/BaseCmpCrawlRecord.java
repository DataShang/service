package com.jyxy.service.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseCmpCrawlRecord<M extends BaseCmpCrawlRecord<M>> extends Model<M> implements IBean {

	public M setFCmpId(java.lang.String fCmpId) {
		set("f_cmp_id", fCmpId);
		return (M)this;
	}
	
	public java.lang.String getFCmpId() {
		return getStr("f_cmp_id");
	}

	public M setFImageCrawled(java.lang.String fImageCrawled) {
		set("f_image_crawled", fImageCrawled);
		return (M)this;
	}
	
	public java.lang.String getFImageCrawled() {
		return getStr("f_image_crawled");
	}

	public M setFWebsiteCrawled(java.lang.String fWebsiteCrawled) {
		set("f_website_crawled", fWebsiteCrawled);
		return (M)this;
	}
	
	public java.lang.String getFWebsiteCrawled() {
		return getStr("f_website_crawled");
	}

	public M setFNewsCrawled(java.lang.String fNewsCrawled) {
		set("f_news_crawled", fNewsCrawled);
		return (M)this;
	}
	
	public java.lang.String getFNewsCrawled() {
		return getStr("f_news_crawled");
	}

}
