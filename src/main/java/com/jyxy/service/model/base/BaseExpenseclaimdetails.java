package com.jyxy.service.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseExpenseclaimdetails<M extends BaseExpenseclaimdetails<M>> extends Model<M> implements IBean {

	public M setID(java.lang.String ID) {
		set("ID", ID);
		return (M)this;
	}
	
	public java.lang.String getID() {
		return getStr("ID");
	}

	public M setTransportation(java.math.BigDecimal Transportation) {
		set("Transportation", Transportation);
		return (M)this;
	}
	
	public java.math.BigDecimal getTransportation() {
		return get("Transportation");
	}

	public M setMeals(java.math.BigDecimal Meals) {
		set("Meals", Meals);
		return (M)this;
	}
	
	public java.math.BigDecimal getMeals() {
		return get("Meals");
	}

	public M setOfficeStationery(java.math.BigDecimal officeStationery) {
		set("Office_Stationery", officeStationery);
		return (M)this;
	}
	
	public java.math.BigDecimal getOfficeStationery() {
		return get("Office_Stationery");
	}

	public M setOthers(java.math.BigDecimal Others) {
		set("Others", Others);
		return (M)this;
	}
	
	public java.math.BigDecimal getOthers() {
		return get("Others");
	}

	public M setTotal(java.math.BigDecimal Total) {
		set("Total", Total);
		return (M)this;
	}
	
	public java.math.BigDecimal getTotal() {
		return get("Total");
	}

	public M setCreateBy(java.lang.String createBy) {
		set("create_by", createBy);
		return (M)this;
	}
	
	public java.lang.String getCreateBy() {
		return getStr("create_by");
	}

	public M setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
		return (M)this;
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public M setUpdateBy(java.lang.String updateBy) {
		set("update_by", updateBy);
		return (M)this;
	}
	
	public java.lang.String getUpdateBy() {
		return getStr("update_by");
	}

	public M setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
		return (M)this;
	}
	
	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public M setRemarks(java.lang.String remarks) {
		set("remarks", remarks);
		return (M)this;
	}
	
	public java.lang.String getRemarks() {
		return getStr("remarks");
	}

	public M setDelFlag(java.lang.String delFlag) {
		set("del_flag", delFlag);
		return (M)this;
	}
	
	public java.lang.String getDelFlag() {
		return getStr("del_flag");
	}

}
