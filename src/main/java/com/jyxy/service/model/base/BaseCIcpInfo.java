package com.jyxy.service.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseCIcpInfo<M extends BaseCIcpInfo<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setCmpId(java.lang.String cmpId) {
		set("cmp_id", cmpId);
		return (M)this;
	}
	
	public java.lang.String getCmpId() {
		return getStr("cmp_id");
	}

	public M setDomain(java.lang.String domain) {
		set("domain", domain);
		return (M)this;
	}
	
	public java.lang.String getDomain() {
		return getStr("domain");
	}

	public M setWebName(java.lang.String webName) {
		set("web_name", webName);
		return (M)this;
	}
	
	public java.lang.String getWebName() {
		return getStr("web_name");
	}

	public M setRecordNo(java.lang.String recordNo) {
		set("record_no", recordNo);
		return (M)this;
	}
	
	public java.lang.String getRecordNo() {
		return getStr("record_no");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.String getStatus() {
		return getStr("status");
	}

	public M setCheckDate(java.util.Date checkDate) {
		set("check_date", checkDate);
		return (M)this;
	}
	
	public java.util.Date getCheckDate() {
		return get("check_date");
	}

	public M setOrgType(java.lang.String orgType) {
		set("org_type", orgType);
		return (M)this;
	}
	
	public java.lang.String getOrgType() {
		return getStr("org_type");
	}

	public M setOrgName(java.lang.String orgName) {
		set("org_name", orgName);
		return (M)this;
	}
	
	public java.lang.String getOrgName() {
		return getStr("org_name");
	}

	public M setCreateBy(java.lang.String createBy) {
		set("create_by", createBy);
		return (M)this;
	}
	
	public java.lang.String getCreateBy() {
		return getStr("create_by");
	}

	public M setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
		return (M)this;
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public M setUpdateBy(java.lang.String updateBy) {
		set("update_by", updateBy);
		return (M)this;
	}
	
	public java.lang.String getUpdateBy() {
		return getStr("update_by");
	}

	public M setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
		return (M)this;
	}
	
	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public M setRemarks(java.lang.String remarks) {
		set("remarks", remarks);
		return (M)this;
	}
	
	public java.lang.String getRemarks() {
		return getStr("remarks");
	}

	public M setDelFlag(java.lang.String delFlag) {
		set("del_flag", delFlag);
		return (M)this;
	}
	
	public java.lang.String getDelFlag() {
		return getStr("del_flag");
	}

}
