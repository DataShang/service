package com.jyxy.service.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseScale<M extends BaseScale<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public M setIndustry(java.lang.String industry) {
		set("industry", industry);
		return (M)this;
	}
	
	public java.lang.String getIndustry() {
		return getStr("industry");
	}

	public M setType(java.lang.String type) {
		set("type", type);
		return (M)this;
	}
	
	public java.lang.String getType() {
		return getStr("type");
	}

	public M setLarge(java.lang.Integer large) {
		set("large", large);
		return (M)this;
	}
	
	public java.lang.Integer getLarge() {
		return getInt("large");
	}

	public M setMiddle(java.lang.Integer middle) {
		set("middle", middle);
		return (M)this;
	}
	
	public java.lang.Integer getMiddle() {
		return getInt("middle");
	}

	public M setSmall(java.lang.Integer small) {
		set("small", small);
		return (M)this;
	}
	
	public java.lang.Integer getSmall() {
		return getInt("small");
	}

	public M setMin(java.lang.Integer min) {
		set("min", min);
		return (M)this;
	}
	
	public java.lang.Integer getMin() {
		return getInt("min");
	}

}
