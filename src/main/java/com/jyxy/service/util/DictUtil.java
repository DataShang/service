package com.jyxy.service.util;

import java.util.List;

import com.jyxy.service.model.SysDict;

public class DictUtil {
    public static final String CACHE_DICT_MAP = "dictMap";

    public static String getDictLabel(String value, String type, String defaultValue) {
        if (StringUtil.isNotNullStr(type) && StringUtil.isNotNullStr(value)) {
            for (SysDict dict : getDictList(type)) {
                if (type.equals(dict.getType()) && value.equals(dict.getValue())) {
                    return dict.getLabel();
                }
            }
        }
        return defaultValue;
    }

    public static String getDictValue(String label, String type, String defaultValue) {
        if (StringUtil.isNotNullStr(type) && StringUtil.isNotNullStr(label)) {
            label = label.trim();
            for (SysDict dict : getDictList(type)) {
                if (type.equals(dict.getType()) && label.equals(dict.getLabel())) {
                    return dict.getValue();
                }
            }
        }
        return defaultValue;
    }

    public static List<SysDict> getDictList(String type) {
        return SysDict.dao.findByCache(CACHE_DICT_MAP, type, "SELECT `value`,label,type from sys_dict WHERE type=?", type);
    }

    public static void main(String[] args) {
//        DbUtil.startPlugin();
        System.out.println(getDictValue("其他联营", "Qylx", ""));
    }
}
