package com.jyxy.service.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5工具类
 * 
 * @author
 * @version 0
 */
public class Md5Util {
	/**
	 * Md
	 * 
	 * @param value
	 *            the value
	 * @return the string
	 */
	public static String md5(String value) {
		try {
			MessageDigest md = MessageDigest.getInstance("md5");
			byte[] e = md.digest(value.getBytes());
			return toHex(e);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return value;
		}
	}

	/**
	 * Md
	 * 
	 * @param bytes
	 *            the bytes
	 * @return the string
	 */
	public static String md5(byte[] bytes) {
		try {
			MessageDigest md = MessageDigest.getInstance("md5");
			byte[] e = md.digest(bytes);
			return toHex(e);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * To hex
	 * 
	 * @param bytes
	 *            the bytes
	 * @return the string
	 */
	private static String toHex(byte bytes[]) {
		StringBuilder hs = new StringBuilder();
		String stmp = "";
		for (int n = 0; n < bytes.length; n++) {
			stmp = Integer.toHexString(bytes[n] & 0xff);
			if (stmp.length() == 1)
				hs.append("0").append(stmp);
			else
				hs.append(stmp);
		}
		return hs.toString();
	}

	public static String encrypt(String value, String encrypt, String timeStamp) {
		char[] values = md5(value + timeStamp).toCharArray();
		char[] encrypts = encrypt.toCharArray();

		if (values.length != encrypts.length) {
			return null;
		}
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < values.length; i++) {
			if (i % 2 == 0) {
				result.append((char) (values[i] + encrypts[i]));
			} else {
				result.append((char) (values[i] - encrypts[i]));
			}
		}
		return md5(result.toString());
	}

	public static void main(String[] args) {
		String t = System.currentTimeMillis() + "";
		// System.out.println("http://127.0.0.1:9090/plant/priceList?sid=%E6%B5%8B%E8%AF%95&timestamp="+t+"&token="+encrypt("","815e1a70010e44a4a6b10299585389ea",t));
		// System.out.println("http://api.datashang.com/plant/priceList?sid=%E6%B5%8B%E8%AF%95&timestamp="
		// + t + "&token="
		// + encrypt("", "815e1a70010e44a4a6b10299585389ea", t));
		// System.out.println("http://api.datashang.com/ent/countDiskSize?sid=%E6%B5%8B%E8%AF%95&timestamp="+t+"&token="+encrypt("","815e1a70010e44a4a6b10299585389ea",t));
		String idS = "CN00100005.2,CN00100005.5";
		System.out.println("http://api.datashang.com/ent/patentList?sid=nxf.ynmaker.com&timestamp=" + t + "&ids=" + idS
				+ "&token=" + encrypt(idS, "1YT7sdkI4pLw2OA6P3T5E2shDKgTBMbR", t));

	}
}