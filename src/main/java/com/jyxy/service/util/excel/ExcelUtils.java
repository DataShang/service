package com.jyxy.service.util.excel;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jyxy.service.model._MappingKit;

public class ExcelUtils {

	public static void main(String[] args) throws IOException {
		try {

			System.out.println("开始启动数据库。。。");
			PropKit.use("default.properties");
			DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"),
					PropKit.get("password").trim());
			ActiveRecordPlugin arp = new ActiveRecordPlugin("mysql", druidPlugin);
			_MappingKit.mapping(arp);

			druidPlugin.start();
			arp.start();

			System.out.println("启动完成，执行插入。。。");
			ExcelDatapath test = new ExcelDatapath();
			List<String[]> list = test.getStoreXlsdata("/Volumes/mac2/2.xls", 0); // 后面的参数代表需要输出哪些列，参数个数可以任意
			for (String[] strings : list) {

				String[] sss = strings;
				System.out.println(sss);
				String uuid = UUID.randomUUID().toString().replaceAll("-", "");
				int ss = Db.update("INSERT INTO worm.nut_price(id,producer) VALUES ('" + uuid + "','" + sss[0] + "');");
			}
			System.out.println("插入完成。。。");
			arp.stop();
			druidPlugin.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

