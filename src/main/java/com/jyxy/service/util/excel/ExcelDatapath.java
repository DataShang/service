package com.jyxy.service.util.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;
public class ExcelDatapath {

	/**
	 * 
	 * @param conn
	 * @throws BiffException
	 * @throws IOException
	 */
	public static List<String[]> getStoreXlsdata(String xlsfilepath, int sheet_page) throws IOException {

		List<String[]> list = new ArrayList<>();
		// 要插入的excel页面
		File excelfile = new File(xlsfilepath);
		Workbook book = null;
		try {
			book = Workbook.getWorkbook(excelfile);
			Sheet sheet = book.getSheet(sheet_page); // 工作簿是从第2页开始,0代表第一页，1代表第二页，2代表第三页
			int count = sheet.getRows();
			int rownum = 0;
			Map<String, Object> map = null;
			int col = 5;
			for (rownum = 1; rownum < count; rownum++) {
				String[] cols = new String[col];
				cols[0] = sheet.getCell(0, rownum).getContents().trim();// 生产厂家
				cols[1] = sheet.getCell(1, rownum).getContents().trim();// 
				cols[2] = sheet.getCell(2, rownum).getContents().trim();// 
				cols[3] = sheet.getCell(3, rownum).getContents().trim();// 
				cols[4] = sheet.getCell(4, rownum).getContents().trim();// 

				
				list.add(cols);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			book.close();
		}
		return list;
	}

}