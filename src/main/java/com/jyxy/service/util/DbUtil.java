package com.jyxy.service.util;

import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;

public class DbUtil
{
  public static DruidPlugin druidPlugin = null;
  public static ActiveRecordPlugin arp = null;
  public static com.jfinal.plugin.ehcache.EhCachePlugin ehCachePlugin = new com.jfinal.plugin.ehcache.EhCachePlugin();
  
  public static DruidPlugin createDruidPlugin(String proFile) { PropKit.use(proFile);
    return druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
  }
  
  public static DruidPlugin createDruidPlugin() { PropKit.use("default.properties");
    return druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
  }
  
  public static void start() { if (druidPlugin != null) {
      druidPlugin.start();
      arp = new ActiveRecordPlugin(druidPlugin);
      
      arp.setDevMode(true);
      arp.setShowSql(true);
      arp.start();
      ehCachePlugin.start();
    } else {
      createDruidPlugin();
      start();
    }
  }
  
  public static void restart() { if (druidPlugin != null) {
      druidPlugin.stop();
      druidPlugin.start();
    }
  }
  
  public static void stop() { if (druidPlugin != null) {
      druidPlugin.stop();
    }
    if (arp != null) {
      arp.stop();
    }
  }
}