package com.jyxy.service.util;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.Engine;
import com.jyxy.service.aop.ServiceInterceptor;
import com.jyxy.service.model._MappingKit;
import com.jyxy.service.web.EntService;
import com.jyxy.service.web.FarmService;
import com.jyxy.service.web.PlantService;
import com.jyxy.service.web.WormCountController;

public class ServiceConfig extends JFinalConfig {
	public static void main(String[] args) {
		JFinal.start("src/main/webapp", 9090, "/");
	}

	public void configConstant(Constants me) {
		PropKit.use("default.properties");
		me.setDevMode(PropKit.getBoolean("devMode", Boolean.valueOf(true)).booleanValue());
	}

	public void configRoute(Routes me) {
		me.add("/worm", WormCountController.class);
		me.add("/plant", PlantService.class);
		me.add("/ent", EntService.class);
		me.add("/farm", FarmService.class);
	}

	public void configEngine(Engine me) {
	}

	public void configPlugin(Plugins me) {
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"),
				PropKit.get("password").trim());
		
		me.add(druidPlugin);
		DruidPlugin plantDruidPlugin = new DruidPlugin(PropKit.get("jdbcUrlPlant"), PropKit.get("user"),
				PropKit.get("password").trim());
		me.add(plantDruidPlugin);
		

		ActiveRecordPlugin arp = new ActiveRecordPlugin("worm", druidPlugin);
		arp.setShowSql(true);
		ActiveRecordPlugin arpPlant = new ActiveRecordPlugin("plant", plantDruidPlugin);

		_MappingKit.mapping(arp);
		me.add(arp);
		me.add(arpPlant);
		me.add(new EhCachePlugin());
	}

	public static DruidPlugin createDruidPlugin() {
		return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
	}

	public void configInterceptor(Interceptors me) {
		me.add(new ServiceInterceptor());
	}

	public void configHandler(Handlers me) {
	}
}
