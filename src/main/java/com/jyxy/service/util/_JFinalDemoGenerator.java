package com.jyxy.service.util;

import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;
import javax.sql.DataSource;

public class _JFinalDemoGenerator
{
  public static DataSource getDataSource()
  {
    DruidPlugin druidPlugin = DbUtil.createDruidPlugin();
    
    druidPlugin.start();
    return druidPlugin.getDataSource();
  }
  
  public static void main(String[] args)
  {
    String baseModelPackageName = "com.jyxy.service.model.base";
    
    String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/jyxy/service/model/base";
    

    String modelPackageName = "com.jyxy.service.model";
    
    String modelOutputDir = baseModelOutputDir + "/..";
    

    Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, 
      modelOutputDir);
    
    generator.setGenerateChainSetter(false);
    
    generator.addExcludedTable(new String[] { "act_evt_log", "act_ge_bytearray", "act_ge_property", "act_hi_actinst", "act_hi_attachment", "act_hi_comment", "act_hi_detail", "act_hi_identitylink", "act_hi_procinst", "act_hi_taskinst", "act_hi_varinst", "act_id_group", "act_id_info", "act_id_membership", "act_id_user", "act_procdef_info", "act_re_deployment", "act_re_model", "act_re_procdef", "act_ru_event_subscr", "act_ru_execution", "act_ru_identitylink", "act_ru_job", "act_ru_task", "act_ru_variable", "cms_article", "cms_article_data", "cms_category", "cms_comment", "cms_guestbook", "cms_link", "cms_site", "f_cmp_investcmp_record", "f_cmp_investpro_record", "gen_scheme", "gen_table", "gen_table_column", "gen_template", "t_cmp_baseinfo", "t_cmp_investment", "t_cmp_lawsuit", "t_cmp_parntner", "t_cmp_se", "t_person_base_info", "test_data", "test_data_child", "test_data_main", "test_tree" });
    
    generator.setGenerateDaoInModel(true);
    
    generator.setGenerateChainSetter(true);
    
    generator.setGenerateDataDictionary(false);
    

    generator.setRemovedTableNamePrefixes(new String[] { "zsj_" });
    
    generator.generate();
  }
}