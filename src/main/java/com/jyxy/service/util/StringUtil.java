package com.jyxy.service.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	public static String QI_CHA_CHA_NULL_STR = "-";//企查查空字符串
    public static String REG_CHINESS = "[\\u4e00-\\u9fa5]+";//中文正则
    public static String REG_NUMBER = "[1-9]\\d*";//数字
    public static String REG_DOUBLE = "[0-9]+([.]{1}[0-9]+){0,1}";//小数
    public static String REG_EMAIL = ".*@.*";//email
    public static String REG_FAX = ".*-.*";//fax
    public static String REG_CSS_URL = "\\((.*)\\)";//url
    public static String REG_URL = "((http|https):\\/\\/){1}.*";//url
    public static String REG_DOMAIN = "((http://)|(https://))?([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}(/)";//url
    public static String REG_PATH = "(\\.|\\/|\\).*";//path
    public static String REG_PHONE = "[0-9]{7,18}";//path
    public static String REG_PHONE_AREA = "0?[0-9]{2,3}";//区号
    private static final char SEPARATOR = '_';
    public static String REG_DATE = "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29)";//日期正则

    public static boolean regVaildion(String str, String reg) {
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static String regGetStr(String str, String reg, String def) {
        return regGetStr(str, reg, def, 0);
    }
    public static String regGetStr(String str, String reg, String def,int group) {
    	Pattern p = Pattern.compile(reg);
    	Matcher m = p.matcher(str);
    	return m.find() ? m.group(group) : def;
    }
    public static List<String> regGetStrArray(String str, String reg, String def) {
    	Pattern p = Pattern.compile(reg);
    	Matcher m = p.matcher(str);
    	List<String> reStrs = new ArrayList<String>();
    	while(m.find()) {
    			reStrs.add(m.group(1));
    	}
    	return reStrs;
    }

    public static boolean isContainNumAndChinese(String str) {
        return regVaildion(str, "\\d" + REG_CHINESS);
    }

    public static boolean isContainChinese(String str) {
        return regVaildion(str, REG_CHINESS);
    }

    public static String replaceChinese(String str) {
        return str.replaceAll(REG_CHINESS, "");
    }

    public static String replaceNum(String str) {
        return str.replaceAll("\\d|\\.", "");
    }

    public static String replaceSign(String str) {
        return str.replaceAll("\\n|\\t", "");
    }

    public static boolean isNotNullStr(Object str) {
        return str != null && !"".equals(str) && !"null".equals(str);
    }

    public static boolean isNullStr(Object str) {
        return !isNotNullStr(str);
    }


    public static String captureFirstStr(String name) {
        char[] cs = name.toCharArray();
        cs[0] -= 32;
        return String.valueOf(cs);
    }

    public static String letterFirstStr(String name) {
        char[] cs = name.toCharArray();
        cs[0] += 32;
        return String.valueOf(cs);
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase("hello_world") == "helloWorld"
     * ("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toCamelCase(String s) {
        if (s == null) {
            return null;
        }

        s = s.toLowerCase();

        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == SEPARATOR) {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase("hello_world") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toCapitalizeCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = toCamelCase(s);
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase("hello_world") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toUnderScoreCase(String s) {
        if (s == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            boolean nextUpperCase = true;

            if (i < (s.length() - 1)) {
                nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
            }

            if ((i > 0) && Character.isUpperCase(c)) {
                if (!upperCase || !nextUpperCase) {
                    sb.append(SEPARATOR);
                }
                upperCase = true;
            } else {
                upperCase = false;
            }

            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    /**
     * 获得一个UUID
     *
     * @return String UUID
     */
    public static String getUUID() {
        String uuid = UUID.randomUUID().toString();
//去掉“-”符号
        return uuid.replaceAll("-", "");
    }

    /**
     * 字符串转化成为16进制字符串
     * @param s
     * @return
     */
    public static String strTo16(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            int ch = (int) s.charAt(i);
            String s4 = Integer.toHexString(ch);
            str = str + s4;
        }
        return str;
    }
    /**
     * unicode转中文
     * @param str
     * @return
     * @author yutao
     * @date 2017年1月24日上午10:33:25
     */
    public static String unicodeToString(String str) {
        Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(str);
        char ch;
        while (matcher.find()) {
            ch = (char) Integer.parseInt(matcher.group(2), 16);
            str = str.replace(matcher.group(1), ch+"" );
        }
        return str;
    }
    public static boolean isQiChaChaNullStr(String str) {
    	return isNullStr(str) || QI_CHA_CHA_NULL_STR.equals(str);
    }
    //	企业类型
    public static void main(String[] args) {
       System.out.println(regGetStr("http://www.xiaomi.com。太极拳。tjqonline.cn。MIUI网络助手。zifei.cn。wali.com.cn。小米生态云。mi-ae.cn。mi-ae.net。小米通讯。mi1.cc。mi-ae.com.cn。mi-ae.com。米聊通讯", "[a-zA-Z]+(.com|.cn)", "",0));
    }
}
