package com.jyxy.service.aop;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.JsonKit;
import com.jyxy.service.model.SysApiLog;
import com.jyxy.service.util.DictUtil;
import com.jyxy.service.util.FullArrangement;
import com.jyxy.service.util.StringUtil;
import com.jyxy.service.web.base.BaseController;

public class ServiceInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		long start = System.currentTimeMillis();
		SysApiLog log = new SysApiLog();
		BaseController controller = inv.getTarget();
		controller.getResponse().addHeader("Access-Control-Allow-Origin", "*");
		HttpServletRequest request = controller.getRequest();	
		Map<String,String[]> param = request.getParameterMap();
		log.setId(StringUtil.getUUID());
		log.setCreateDate(new Date());
		log.setHost(request.getRemoteHost());
		log.setPath(request.getServletPath());
		log.setParam(JsonKit.toJson(request.getParameterMap()));
		log.setUserAgent(request.getHeader("User-Agent"));
		log.setMethod(request.getMethod());
		if(request.getServletPath().indexOf("plant")!=-1) {
			inv.invoke();
			log.setStatus("2");
		}else {
			if(!param.containsKey("sid")) {
				controller.rendError(null, "系统标识不能为空！");
			}else if(!param.containsKey("timestamp")) {
				controller.rendError(null, "时间戳不能为空");
			}else if(!param.containsKey("token")) {
				controller.rendError(null, "token不能为空");
			}else {
				log.setSystem(param.get("sid")[0]);
				String sidKey = DictUtil.getDictValue(param.get("sid")[0], "systemId", null);
				if(sidKey==null) {
					controller.rendError(null, "无效的系统标识");
				}else {
//					String pStr = "";
//					String[] params=request.getQueryString().split("&");
//					for(String tmp:params) {
//						String[] tmpParam = tmp.split("=");
//						if(!"sid".equals(tmpParam[0]) && !"token".equals(tmpParam[0]) && !"timestamp".equals(tmpParam[0])) {
//							pStr+=param.get(tmpParam[0])[0];
//						}
//					}
					String token = param.get("token")[0];
					String time = param.get("timestamp")[0];
					
					List<String> parlist=new ArrayList<String>();
					for (String key : param.keySet()) {
						if(!"sid".equals(key)&&!"token".equals(key)&&!"timestamp".equals(key)){
							parlist.add(param.get(key)[0]);
						}
					}
					String[] strs=parlist.toArray(new String[parlist.size()]);					
					List<String> md5list=FullArrangement.getMd5List(strs,sidKey,time);
//			接口时间戳与系统
					if(start-Long.parseLong(time)>10*60*1000) {
						controller.rendError(null, "token过期");
					}else if(token!=null && md5list.contains(token)) {
						inv.invoke();
						log.setStatus("1");
					}else {
						controller.rendError(null, "token校验失败");
					}
				}
			}
		}
		log.setReparama(JsonKit.toJson(controller.getReMap()));
		long end = System.currentTimeMillis();
		log.setUseTime(end-start);
		log.save();
	}
}

