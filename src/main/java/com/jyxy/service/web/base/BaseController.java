package com.jyxy.service.web.base;

import com.jfinal.core.Controller;
import java.util.HashMap;
import java.util.Map;

public class BaseController extends Controller {
	public Map<String, Object> reMap = new HashMap<String, Object>();
	
	public Map<String,Object> getReMap(){
		return reMap;
	}

	public void rendSuccess(Object data) {
		this.reMap.put("success", 1);
		this.reMap.put("data", data);
		renderJson(this.reMap);
	}

	public void rendError(Object data, String msg) {
		this.reMap.put("success", 0);
		this.reMap.put("msg", msg);
		this.reMap.put("data", data);
		renderJson(this.reMap);
	}
}
