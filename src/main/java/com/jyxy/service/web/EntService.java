package com.jyxy.service.web;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jyxy.service.model.CCompanyPatent;
import com.jyxy.service.util.StringUtil;
import com.jyxy.service.web.base.BaseController;

public class EntService extends BaseController {
	public void patentList() {
		String name = this.getPara("name");
		String ipc = this.getPara("ipc");
		String type = this.getPara("type");
		String city_code = this.getPara("city_code");
		String ids = this.getPara("ids");
		String tags = this.getPara("tags");
		int pageSize = this.getParaToInt("page_size", 10);
		String where = " 1=1 ";
		if (StringUtil.isNotNullStr(name)) {
			where += "and name like '%" + name + "%'";
		}
		if (StringUtil.isNotNullStr(ipc)) {
			where += "and ipc like '%" + ipc + "%'";
		}
		if (StringUtil.isNotNullStr(type)) {
			where += "and type like '%" + type + "%'";
		}
		if (StringUtil.isNotNullStr(city_code)) {
			String codeCOndition = "";
			if (city_code.indexOf(",") > -1) {
				String[] cityCodes = city_code.split(",");
				for (String code : cityCodes) {
					codeCOndition += "'" + code + "',";
				}
				codeCOndition = codeCOndition.substring(0, codeCOndition.length() - 1);
			} else {
				codeCOndition = "'" + city_code + "'";
			}

			where += "and city_code in(" + codeCOndition + ")";
		}
		if (StringUtil.isNotNullStr(ids)) {
			String idCondition = "";
			if (ids.indexOf(",") > -1) {
				String[] idsArray = ids.split(",");
				for (String id : idsArray) {
					idCondition += "'" + id + "',";
				}
				idCondition = idCondition.substring(0, idCondition.length() - 1);
			} else {
				idCondition = "'" + ids + "'";
			}
			where += "and id in(" + idCondition + ")";
		}
		if (StringUtil.isNotNullStr(tags)) {
			String tagCondition = "";
			if (tags.indexOf(",") > -1) {
				String[] tagsArray = tags.split(",");
				for (String id : tagsArray) {
					tagCondition += "'" + id + "',";
				}
				tagCondition = tagCondition.substring(0, tagCondition.length() - 1);
			} else {
				tagCondition = "'" + tags + "'";
			}
			where += "and id in(SELECT business_id FROM dm.mark_lables WHERE id in(" + tagCondition + "))";
		}
		Map<String, Object> reMap = new HashMap<>();
		int pageNum = this.getParaToInt("page_num", 1);
		String sql = "SELECT id,`name`,patent_id,apply_date,public_date,type,legal_status,ipc FROM zsj_c_company_patent WHERE  "
				+ where;
		System.out.println("专利接口sql：" + sql);
		// Page<CCompanyPatent> page = CCompanyPatent.dao.paginate(pageNum, pageSize,
		// "SELECT id,`name`,patent_id,apply_date,public_date,type,legal_status,ipc",
		// " FROM zsj_c_company_patent WHERE id in (SELECT id FROM zsj_c_company_patent
		// where" + where + ")");

		Page<CCompanyPatent> page = CCompanyPatent.dao.paginate(pageNum, pageSize,
				"SELECT id,`name`,patent_id,apply_date,public_date,type,legal_status,ipc",
				" FROM zsj_c_company_patent WHERE " + where);
		reMap.put("total", page.getTotalRow());
		reMap.put("data", page.getList());
		rendSuccess(reMap);
	}

	public void jobList() {
		int pageSize = this.getParaToInt("page_size", 10);
		Map<String, Object> reMap = new HashMap<>();
		int pageNum = this.getParaToInt("page_num", 1);
		Page<CCompanyPatent> page = CCompanyPatent.dao.paginate(pageNum, pageSize, "SELECT *",
				"FROM zsj_c_company_job ORDER BY salary,real_pub_date");
		reMap.put("total", page.getTotalRow());
		reMap.put("data", page.getList());
		rendSuccess(reMap);
	}

	public void lableList() {
		int pageSize = this.getParaToInt("page_size", 10);
		Map<String, Object> reMap = new HashMap<>();
		int pageNum = this.getParaToInt("page_num", 1);
		Page<Record> page = Db.paginate(pageNum, pageSize, "SELECT *", "FROM dm.lables");
		reMap.put("total", page.getTotalRow());
		reMap.put("data", page.getList());
		rendSuccess(reMap);
	}

	public void patentDetail() {
		String id = this.getPara("id");
		CCompanyPatent patent = CCompanyPatent.dao.findById(id);
		rendSuccess(patent);
	}

	public void countService() {
		Map<String, Object> sqlmap = new HashMap<>();
		long count = Db.queryLong("SELECT COUNT(1) FROM `sys_api_log`");
		Long percent = Db.queryLong(
				"SELECT floor(((SELECT COUNT(1) FROM `sys_api_log` WHERE create_date BETWEEN date_sub(curdate(), INTERVAL 7 DAY) AND curdate())-(SELECT COUNT(1) FROM `sys_api_log` WHERE create_date BETWEEN date_sub(curdate(), INTERVAL 14 DAY) AND date_sub(curdate(), INTERVAL 7 DAY)))/(SELECT COUNT(1) FROM `sys_api_log` WHERE create_date BETWEEN date_sub(curdate(), INTERVAL 14 DAY) AND date_sub(curdate(), INTERVAL 7 DAY))*100)");
		sqlmap.put("count", count);
		sqlmap.put("percent", percent == null ? 0 : percent);
		rendSuccess(sqlmap);
	}

	public void countEnt() {
		Map<String, Object> sqlmap = new HashMap<>();
		long count = Db.queryLong("SELECT COUNT(1) FROM `zsj_c_company`");
		Long percent = Db.queryLong(
				"SELECT floor (((SELECT SUM(day_increment) FROM zsj_table_insert_record WHERE  table_name='zsj_c_company' AND record_date BETWEEN date_sub(curdate(), INTERVAL 7 DAY) AND curdate())-(SELECT SUM(day_increment) FROM zsj_table_insert_record WHERE  table_name='zsj_c_company' AND record_date BETWEEN date_sub(curdate(), INTERVAL 14 DAY) AND date_sub(curdate(), INTERVAL 7 DAY)))/(SELECT SUM(day_increment) FROM zsj_table_insert_record WHERE  table_name='zsj_c_company' AND record_date BETWEEN date_sub(curdate(), INTERVAL 14 DAY) AND date_sub(curdate(), INTERVAL 7 DAY))*100)");
		sqlmap.put("count", count);
		sqlmap.put("percent", percent == null ? 0 : percent);
		rendSuccess(sqlmap);
	}

	public void countDiskSize() {
		Map<String, Object> sqlmap = new HashMap<>();
		long count = Db.queryLong(
				"SELECT SUM(size) FROM `zsj_linux_size` WHERE create_date=(SELECT MAX(create_date) FROM `zsj_linux_size`)");
		Long percent = Db.queryLong(
				"SELECT floor (((SELECT COUNT(1) FROM `zsj_linux_size` WHERE create_date BETWEEN date_sub(curdate(), INTERVAL 7 DAY) AND curdate())-(SELECT COUNT(1) FROM `zsj_linux_size` WHERE create_date BETWEEN date_sub(curdate(), INTERVAL 14 DAY) AND date_sub(curdate(), INTERVAL 7 DAY)))/(SELECT COUNT(1) FROM `zsj_linux_size` WHERE create_date BETWEEN date_sub(curdate(), INTERVAL 14 DAY) AND date_sub(curdate(), INTERVAL 7 DAY))*100)");
		sqlmap.put("count", count);
		sqlmap.put("percent", percent == null ? 0 : percent);
		rendSuccess(sqlmap);
	}
}
