package com.jyxy.service.web;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.plugin.activerecord.Page;
import com.jyxy.service.model.CCompanyPatent;
import com.jyxy.service.web.base.BaseController;

public class FarmService extends BaseController {
	public  void priceYnList() {
		int pageSize = this.getParaToInt("page_size", 10);
		Map<String, Object> reMap = new HashMap<>();
		int pageNum = this.getParaToInt("page_num", 1);
		String sortField=this.getPara("sort_field", "pub_date");
		String sortMethod=this.getPara("sort_method", "");
		String name=this.getPara("name", "");
		String nameField=name.matches("[a-zA-Z]+")?"`spell`":"`name`";
		CCompanyPatent res=CCompanyPatent.dao.findFirst("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='yn_vegetables_price' AND COLUMN_NAME=?",sortField);
		if(res==null){
			rendError(reMap,"字段不存在");
			return;
		}
		Page<CCompanyPatent> page = CCompanyPatent.dao.paginate(pageNum, 
				pageSize, "SELECT *",
				"FROM farm.yn_vegetables_price WHERE pub_date=(SELECT MAX(pub_date) FROM farm.yn_vegetables_price) and "+nameField+" LIKE ? "
				+ "ORDER BY "+sortField+" "+sortMethod,"%"+name+"%");
		reMap.put("total", page.getTotalRow());
		reMap.put("data", page.getList());
		rendSuccess(reMap);
	}
}
